package tec2.proyecto.eva2_09_sqlite_2;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class Main extends AppCompatActivity {

    SQLiteDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        database = openOrCreateDatabase("mi_BD",MODE_PRIVATE,null);

        try{
            database.execSQL("create table datos(datosId integer primary key autoincrement, nombre text, apellido text);");
        } catch (SQLiteException e){
            e.printStackTrace();
        }
        ContentValues values = new ContentValues();
        values.put("name","Angel Xavier");
        values.put("apellido","Cordova Urueta");
        long iClave;
        iClave = database.insert("datos",null,values);
        values.clear();
        Toast.makeText(this, "Última llave "+ iClave, Toast.LENGTH_SHORT).show();
        

    }
}
