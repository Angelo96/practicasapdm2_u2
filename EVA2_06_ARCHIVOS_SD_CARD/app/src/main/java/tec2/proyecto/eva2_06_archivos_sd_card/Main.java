package tec2.proyecto.eva2_06_archivos_sd_card;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Main extends AppCompatActivity {

    EditText edtxText;
    String rutaSDcard;
    final static int WRITE_PERMISSION=100;
    boolean bWriteAndRead = false;
    final String ARCHIVO = "archivou.txt";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edtxText = findViewById(R.id.edtxTexto);
        rutaSDcard = Environment.getExternalStorageDirectory().getAbsolutePath();
        Toast.makeText(this, rutaSDcard, Toast.LENGTH_SHORT).show();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},WRITE_PERMISSION);
        } else {
            bWriteAndRead = true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == WRITE_PERMISSION){
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED){

            }
        }
    }

    public void onClickWrite(View v){
        if (bWriteAndRead){
            Toast.makeText(this, "CON PERMISO", Toast.LENGTH_SHORT).show();
            String[] asCade = edtxText.getText().toString().split("\n");
            try {
                FileOutputStream fosStream = new FileOutputStream(rutaSDcard + "/" + ARCHIVO);
                //OutputStream osStream = openFileOutput(ARCHIVO,0);
                OutputStreamWriter oswWrite = new OutputStreamWriter(fosStream);
                BufferedWriter bwEscribe = new BufferedWriter(oswWrite);
                for (int i =0; i <asCade.length;i++){
                    bwEscribe.append(asCade[i]);
                    bwEscribe.append("\n");
                }
                bwEscribe.close();
                edtxText.setText("");
            } catch (IOException e){
                e.printStackTrace();
            }

        } else {
            Toast.makeText(this, "SIN PERMISO", Toast.LENGTH_SHORT).show();
        }
    }

    public void  onClickRead(View v){
        if (bWriteAndRead){
            String sCade;
            try{
                edtxText.setText("");
                FileInputStream fisStream = new FileInputStream(rutaSDcard + "/" +ARCHIVO);
                //InputStream isStream = openFileInput("ARCHIVO");
                InputStreamReader isrReader = new InputStreamReader(fisStream);
                BufferedReader brReader = new BufferedReader(isrReader);
                while((sCade = brReader.readLine()) != null){
                    edtxText.append(sCade);
                    edtxText.append("\n");
                }
                brReader.close();
            } catch (FileNotFoundException e){e.printStackTrace();} catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
