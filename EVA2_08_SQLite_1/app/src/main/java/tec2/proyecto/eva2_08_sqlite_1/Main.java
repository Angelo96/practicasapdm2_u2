package tec2.proyecto.eva2_08_sqlite_1;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class Main extends AppCompatActivity {

    SQLiteDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //ABRIR BASE DE DATOS Y CREEARLA SI NO EXISTE
        database = openOrCreateDatabase("mi_BD", MODE_PRIVATE, null);
        //CREAR TABLA
        try{
            database.execSQL("create table miTabla (id integer PRIMARY KEY autoincrement, nombre text, apellido text);");
        } catch (SQLiteException e){
            e.printStackTrace();
        }

        //INSERTAR VALORES
        try{
            /*database.execSQL("insert into miTabla(id,nombre,apellido) values ('Angelo','Cordova')");
            database.execSQL("insert into miTabla(id,nombre,apellido) values ('Lalo','Landa')");
            database.execSQL("insert into miTabla(id,nombre,apellido) values ('Uachuma','Ralagalleta')");*/

            //NOT TESTED YEt
            //database.execSQL("delete from miTabla where id = 3");

            //CURSOR PARA AÑMACENAR CONSULTA
            Cursor c1 = database.rawQuery("select * from miTabla",null);
            c1.moveToFirst();
            while (!c1.isAfterLast()){
                Toast.makeText(this, c1.getString(c1.getColumnIndex("nombre")) + " " +c1.getString(c1.getColumnIndex("apellido")),
                        Toast.LENGTH_SHORT).show();
                c1.moveToNext();

            }

        } catch (SQLiteException e){
            e.printStackTrace();
        }

    }
}
