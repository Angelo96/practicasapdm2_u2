package tec2.proyecto.eva2_13_clima;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Main extends AppCompatActivity {

    TextView textView;
    final String sURL = "http://api.openweathermap.org/data/2.5/group?id=4014338,1835848,4149077&units=metric&appid=ac30fbc76d6479eb1cfa725af41b814f";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = findViewById(R.id.tvSHow);
        new ClimaApp().execute();
    }

    class ClimaApp extends AsyncTask<Void, Void,String>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... voids) {
            String sResul="";

            try{
                URL url = new URL(sURL);
                HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                if(httpURLConnection.getResponseCode()==HttpURLConnection.HTTP_OK){
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                    sResul = bufferedReader.readLine();
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return sResul;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            JSONArray jsonArray = null;
            if (s!=null){
                try{
                    JSONObject jsonObjectCd = new JSONObject(s);
                    jsonArray = jsonObjectCd.getJSONArray("list");


                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonCD = jsonArray.getJSONObject(i);
                        textView.append("Ciudad "+jsonCD.getString("name")+"\n");

                        JSONObject jsonMain = jsonCD.getJSONObject("main");
                        textView.append("Temperatura: "+jsonMain.getString("temp")+"\n");

                        JSONArray jsonWeat = jsonCD.getJSONArray("weather");
                        for (int j = 0; j < jsonWeat.length(); j++) {
                            JSONObject jsonYA = jsonWeat.getJSONObject(j);
                            textView.append("\tClima: "+jsonYA.getString("main")+"\n");
                            textView.append("\tDescripción: "+jsonYA.getString("description")+"\n\n");
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }


    }
}
