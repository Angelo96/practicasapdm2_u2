package tec2.proyecto.eva2_01_shared_preferences;

import android.app.Activity;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

public class Main extends AppCompatActivity {

    EditText etName, etApe, etEdad;
    SharedPreferences spShare;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        etName = (EditText) findViewById(R.id.edtxName);
        etApe = (EditText) findViewById(R.id.edtxApe);
        etEdad = (EditText) findViewById(R.id.edtxEdad);

        //LEER NUESTRO NUESTRO SHARED PREFERENCE Y ACUALIZAR LA INTERFAZ

        spShare = getSharedPreferences("mis_datos", Activity.MODE_PRIVATE);
        etName.setText(spShare.getString("NOMBRE","no name"));
        etApe.setText(spShare.getString("APE","no apelido"));
        etEdad.setText(spShare.getInt("EDAD",0)+"");
    }

    @Override
    protected void onPause() {
        super.onPause();
        //AQUI VAMOS A GUARDAR LOS DATOS
        String sNom, sApe;
        int iEdad;
        sNom = etName.getText().toString();
        sApe = etApe.getText().toString();
        iEdad = Integer.parseInt(etEdad.getText().toString());

        SharedPreferences.Editor speEditor = spShare.edit();
        speEditor.putString("NOMBRE",sNom);
        speEditor.putString("APE",sApe);
        speEditor.putInt("EDAD",iEdad);
        speEditor.commit();
    }
}
