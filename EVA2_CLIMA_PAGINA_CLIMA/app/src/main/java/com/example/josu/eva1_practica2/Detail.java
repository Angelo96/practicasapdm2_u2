package com.example.josu.eva1_practica2;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class Detail extends AppCompatActivity {
    ImageView imgVwCli;
    TextView txtVwCd, txtVwCli, txtVwDet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        imgVwCli = findViewById(R.id.imgVwCli);
        txtVwCd = findViewById(R.id.txtVwCd);
        txtVwCli =  findViewById(R.id.txtVwCli);
        txtVwDet =  findViewById(R.id.txtVwDet);
        //Leer los datos
        Intent inDatos = getIntent();
        int Ima = inDatos.getIntExtra("IMAGEN",R.drawable.sunny);
        String Cd = inDatos.getStringExtra("CIUDAD");
        String Temp = inDatos.getStringExtra("TEMP");
        String Cli = inDatos.getStringExtra("CLIMA");
        imgVwCli.setImageResource(Ima);
        txtVwCd.setText(Cd);
        txtVwCli.setText(Temp);
        txtVwDet.setText(Cli);
    }
}

