package com.example.josu.eva1_practica2;

/**
 * Created by Josué on 27/02/2018.
 */

public class DatosClima {
    String Ciudad, Temp, Clima;
    int Imagen;
    static DatosClima[] dcDatos = {
            new DatosClima("Chihuahua","15ºC","Nublado",R.drawable.cloudy),
            new DatosClima("Aquiles Serdan","10ºC","Nublado",R.drawable.cloudy),
            new DatosClima("Parral","20ºC","Soleado",R.drawable.sunny),
            new DatosClima("Tec 2","9ºC","Lluvioso",R.drawable.rainy),
            new DatosClima("Juarez","25ºC","Soleado",R.drawable.sunny)
    };

    public DatosClima(String ciudad, String temp, String clima, int imagen) {
        this.Ciudad = ciudad;
        this.Temp = temp;
        this.Clima = clima;
        this.Imagen = imagen;
    }
}
