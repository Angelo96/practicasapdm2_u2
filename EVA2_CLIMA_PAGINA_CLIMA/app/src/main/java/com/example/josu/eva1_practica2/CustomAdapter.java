package com.example.josu.eva1_practica2;
import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;


public class CustomAdapter extends ArrayAdapter<DatosClima> {
    Context cntApp;
    int Layout;
    DatosClima[] dcDatos;

    public CustomAdapter(Context context, int resource, DatosClima[] objects) {
        super(context, resource, objects);
        cntApp= context;
        Layout=resource;
        dcDatos=objects;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View Fila = convertView;
        ImageView imgClima;
        TextView txtVwCd, txtVwTemp,txtVwCli;
        if(Fila==null){
            LayoutInflater liCrearLayout = ((Activity)cntApp).getLayoutInflater();
            Fila = liCrearLayout.inflate(Layout, parent, false);
        }
        imgClima = (ImageView) Fila.findViewById(R.id.imgVwClima);
        txtVwCd = (TextView) Fila.findViewById(R.id.txtVwCd);
        txtVwTemp = (TextView) Fila.findViewById(R.id.txtVwTemp);
        txtVwCli = (TextView) Fila.findViewById(R.id.txtVwCli);
        DatosClima dcOb = dcDatos[position];
        imgClima.setImageResource(dcOb.Imagen);
        txtVwCd.setText(dcOb.Ciudad);
        txtVwTemp.setText(dcOb.Temp);
        txtVwCli.setText(dcOb.Clima);


        return Fila;
    }
}
