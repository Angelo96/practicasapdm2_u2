package com.example.josu.eva1_practica2;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentDetail extends android.support.v4.app.Fragment {
    View view;
    TextView txtVwFrCd, txtVwFrCli, txtVwFrDet;
    ImageView imgVwfrCli;
    DatosClima datosClima;


    public FragmentDetail() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_fragment_detail, container, false);
        txtVwFrCd = view.findViewById(R.id.txtVwFrCd);
        txtVwFrCli = view.findViewById(R.id.txtVwFrCli);
        txtVwFrDet = view.findViewById(R.id.txtVwFrDet);
        imgVwfrCli = view.findViewById(R.id.imgVwFr);
        return view;
    }

    public void onMessageFromMain(int postition){
    txtVwFrCd.setText(datosClima.dcDatos[postition].Ciudad);
    txtVwFrCli.setText(datosClima.dcDatos[postition].Temp);
    txtVwFrDet.setText(datosClima.dcDatos[postition].Clima);
    imgVwfrCli.setImageResource(datosClima.dcDatos[postition].Imagen);

    }

}
