package com.example.josu.eva1_practica2;

import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class Main extends AppCompatActivity implements ListView.OnItemClickListener {
    ListView lstVwClima;
    DatosClima datosClima;
    boolean flag;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lstVwClima= findViewById(R.id.lstVwClima);
        lstVwClima.setAdapter(new CustomAdapter(this,R.layout.lista_clima,datosClima.dcDatos));
        lstVwClima.setOnItemClickListener(this);
        View detail = findViewById(R.id.frmTarget);

        flag = detail != null && detail.getVisibility() == View.VISIBLE;
        if(flag){
            FragmentDetail fragmentDetail =(FragmentDetail)getSupportFragmentManager().findFragmentById(R.id.frmTarget);
            if(fragmentDetail == null){
                fragmentDetail = new FragmentDetail();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.add(R.id.frmTarget, fragmentDetail);
                fragmentTransaction.commit();
            }

        }

    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if(flag){
            FragmentDetail fragmentDetail = (FragmentDetail)getSupportFragmentManager().findFragmentById(R.id.frmTarget);
            if(fragmentDetail == null){
                fragmentDetail = new FragmentDetail();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frmTarget,fragmentDetail);
                fragmentTransaction.commit();
            }
            fragmentDetail.onMessageFromMain(position);

        }else{
            Intent inDetalle = new Intent(this, Detail.class);
            int Img = datosClima.dcDatos[position].Imagen;
            String sCd = datosClima.dcDatos[position].Ciudad;
            String sTemp = datosClima.dcDatos[position].Temp;
            String sClima = datosClima.dcDatos[position].Clima;
            inDetalle.putExtra("IMAGEN",Img);
            inDetalle.putExtra("CIUDAD",sCd);
            inDetalle.putExtra("TEMP",sTemp);
            inDetalle.putExtra("CLIMA",sClima);
            startActivity(inDetalle);
        }


    }
}
