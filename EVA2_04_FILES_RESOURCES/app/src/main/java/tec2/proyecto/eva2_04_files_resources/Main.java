package tec2.proyecto.eva2_04_files_resources;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Main extends AppCompatActivity {

    TextView tvArchivo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvArchivo = findViewById(R.id.tvArchivo);
    }

    @Override
    protected void onStart() {
        super.onStart();
        //LEER EL ARCHIVO TXT
        //ABRIR ARCHIVO:
        InputStream isStream = getResources().openRawResource(R.raw.archivo1);
        //ACCEDER EL CONTENIDO
        InputStreamReader isrRead = new InputStreamReader(isStream);
        //TRABAJAR CON TEXTO
        BufferedReader brReader = new BufferedReader(isrRead);

        try {
            String sCade;
            while ((sCade = brReader.readLine()) != null){
                tvArchivo.append(sCade);
                tvArchivo.append("\n");
            }
            brReader.close();
        } catch (IOException e){

        }
    }
}
