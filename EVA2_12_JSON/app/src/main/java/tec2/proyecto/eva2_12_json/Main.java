package tec2.proyecto.eva2_12_json;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class Main extends AppCompatActivity {

    TextView tvDatos;
    EditText etNoProd;
    final private String sDB_URL = "http://localhost/testMoviles/config.inc.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvDatos = findViewById(R.id.tvShow);
        etNoProd = findViewById(R.id.etNoProd);
    }

    public void OnClickMain(View v){
        new JSCONconnect().execute(etNoProd.getText().toString());
    }

    class JSCONconnect extends AsyncTask<String, Void, String>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            JSONArray jaProductos = null;
            if (s != null){
                try {
                    JSONObject object = new JSONObject(s);
                    jaProductos = object.getJSONArray("product");
                    for (int i = 0; i < jaProductos.length();i++){
                        JSONObject jsonObjectRegistro = jaProductos.getJSONObject(i);
                        tvDatos.append("Clave = " + jsonObjectRegistro.getString("productid")+ "\n");
                        tvDatos.append("Producto = " + jsonObjectRegistro.getString("productname")+ "\n");
                        tvDatos.append("Precio unitario = " + jsonObjectRegistro.getString("unitprice")+ "\n");

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            String sResult = "";
            String sClave;

            try{
                sClave = "?pid="+ URLEncoder.encode(strings[0],"UTF-8");
                URL url = new URL(sDB_URL+sClave);
                HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                //VERIFICAMOS SI TODO SALIO BIEN
                if (connection.getResponseCode() == HttpURLConnection.HTTP_OK){
                    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    sResult = reader.readLine();
                }
            } catch (Exception e){
                e.printStackTrace();
            }
            return sResult;
        }
    }
}
