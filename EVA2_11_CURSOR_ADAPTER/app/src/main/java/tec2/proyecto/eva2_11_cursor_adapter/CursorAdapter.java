package tec2.proyecto.eva2_11_cursor_adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Angel Cordova Urueta on 11/04/2018.
 */

public class CursorAdapter extends android.widget.CursorAdapter {

    public CursorAdapter(Context context, Cursor c) {
        super(context, c);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return LayoutInflater.from(context).inflate(R.layout.layout_datos,viewGroup,false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView tvApellido, tvNombre;
        tvApellido = view.findViewById(R.id.tvApe);
        tvNombre = view.findViewById(R.id.tvNom);
        tvNombre.setText(cursor.getString(cursor.getColumnIndex("nombre")));
        tvApellido.setText(cursor.getString(cursor.getColumnIndex("apellido")));
    }
}
