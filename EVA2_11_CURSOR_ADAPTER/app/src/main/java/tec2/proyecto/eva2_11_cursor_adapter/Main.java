package tec2.proyecto.eva2_11_cursor_adapter;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.io.File;

public class Main extends AppCompatActivity {

    ListView listView;
    SQLiteDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = findViewById(R.id.lvDatos);

        //CREAR LA BASE DE DATOS en la SDcard
        String sRuta = Environment.getExternalStorageDirectory().getPath();
        String sDir = "eva2_11_cursor_adap";
        File fRuta = new File(sRuta+"/"+sDir+"/");
        if (!fRuta.exists()){//EXISTE LA RUTA?
            fRuta.mkdir();
        }
        String sRutaBD = sRuta + "/" + sDir + "/" + "mi_bd";
        database = SQLiteDatabase.openDatabase(sRutaBD,null,SQLiteDatabase.CREATE_IF_NECESSARY);
        //CREAR LA TABLA
        try{
            database.execSQL("create table datos(id integer primary key autoincrement, nombre text, apellido text);");
        } catch (SQLiteException e){
            e.printStackTrace();
        }
        //AGREGAR DATOS
        try{
            database.execSQL("insert into datos(nombre,apellido)values('Angel Xavier','Cordova Urueta');");
            database.execSQL("insert into datos(nombre,apellido)values('Angel sdf','Cordova dfgh');");
            database.execSQL("insert into datos(nombre,apellido)values('Angel hghj','dfg Urueta');");
            database.execSQL("insert into datos(nombre,apellido)values('Angel rty','Cordova fgh');");
            database.execSQL("insert into datos(nombre,apellido)values('Angel ghj','jghj Urueta');");
            database.execSQL("insert into datos(nombre,apellido)values('Angel fgh','Cordova euey');");
            database.execSQL("insert into datos(nombre,apellido)values('Angel dfg','styrty Urueta');");
        }catch (SQLiteException e){e.printStackTrace();}
        //CREAR CURSOR
        Cursor c1 = database.rawQuery("select id as _id, nombre, apellido from datos;",null);
        //VINCULAR EL CURSOR A LA LISTA
        CursorAdapter adapter = new CursorAdapter(this, c1);
        listView.setAdapter(adapter);

    }
}
