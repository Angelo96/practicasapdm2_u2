package tec2.proyecto.eva2_14_json_apirest;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Main extends AppCompatActivity {

    TextView textView;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = findViewById(R.id.tvMostrar);
        button = findViewById(R.id.btn1);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new JSONConnect().execute();
            }
        });
    }

    class JSONInsert extends AsyncTask<String,Void,String>{

        @Override
        protected String doInBackground(String... strings) {



            return null;

        }
    }

    class JSONConnect extends AsyncTask<Void, Void,String> {
        final String sEnlace = "http://192.168.137.1:3000/Tasks/";

        @Override
        protected String doInBackground(Void... voids) {
            String sResul="";

            try{
                URL url = new URL(sEnlace);
                HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                if(httpURLConnection.getResponseCode()==HttpURLConnection.HTTP_OK){
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                    sResul = bufferedReader.readLine();
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return sResul;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            JSONArray jsonArray = null;
            if (s!=null){
                try{

                    //CORREGIR, ALGUN DIA HAHAHAHAHA
                    JSONObject jsonObjectCd = new JSONObject(s);
                    jsonArray = jsonObjectCd.getJSONArray("list");


                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonCD = jsonArray.getJSONObject(i);
                        textView.append("Ciudad "+jsonCD.getString("name")+"\n");

                        JSONObject jsonMain = jsonCD.getJSONObject("main");
                        textView.append("Temperatura: "+jsonMain.getString("temp")+"\n");

                        JSONArray jsonWeat = jsonCD.getJSONArray("weather");
                        for (int j = 0; j < jsonWeat.length(); j++) {
                            JSONObject jsonYA = jsonWeat.getJSONObject(j);
                            textView.append("\tClima: "+jsonYA.getString("main")+"\n");
                            textView.append("\tDescripción: "+jsonYA.getString("description")+"\n\n");
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
}
