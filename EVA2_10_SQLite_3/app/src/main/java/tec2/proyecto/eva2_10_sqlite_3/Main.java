package tec2.proyecto.eva2_10_sqlite_3;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Main extends AppCompatActivity {

    SQLiteDatabase database;
    TextView tvSh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvSh = findViewById(R.id.tvShow);
        database = openOrCreateDatabase("mi_db",MODE_PRIVATE,null);

        try{
            database.execSQL("create table datos(id integer primary key autoincrement, nombre text);");
        }catch (SQLiteException e){
            e.printStackTrace();
        }

        //
        database.beginTransaction();
        try{
            database.execSQL("insert into datos(nombre) values ('Juan');");
            database.execSQL("insert into datos(nombre) values ('Ana');");
            database.execSQL("insert into datos(nombre) values ('Pedro');");
            database.execSQL("insert into datos(nombre) values ('Lalo');");
            database.setTransactionSuccessful();
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }

        Cursor cursor = database.rawQuery("select * from datos",null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            tvSh.append(cursor.getString(cursor.getColumnIndex("nombre")));
            tvSh.append("\n");
            cursor.moveToNext();
        }
    }
}
