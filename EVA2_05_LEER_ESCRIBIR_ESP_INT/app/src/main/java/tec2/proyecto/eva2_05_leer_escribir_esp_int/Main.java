package tec2.proyecto.eva2_05_leer_escribir_esp_int;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class Main extends AppCompatActivity {

    EditText edxtTxt;
    Button btnSave;
    final String ARCHIVO = "archivou.txt";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edxtTxt = findViewById(R.id.edtxTexto);

        btnSave = findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] asCade = edxtTxt.getText().toString().split("\n");
                try {
                    OutputStream osStream = openFileOutput(ARCHIVO,0);
                    OutputStreamWriter oswWrite = new OutputStreamWriter(osStream);
                    BufferedWriter bwEscribe = new BufferedWriter(oswWrite);
                    for (int i =0; i <asCade.length;i++){
                        bwEscribe.append(asCade[i]);
                        bwEscribe.append("\n");
                    }
                    bwEscribe.close();
                } catch (IOException e){
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        String sCade;
        try{
            edxtTxt.setText("" +
                    "");
            InputStream isStream = openFileInput("ARCHIVO");
            InputStreamReader isrReader = new InputStreamReader(isStream);
            BufferedReader brReader = new BufferedReader(isrReader);
            while((sCade = brReader.readLine()) != null){
                edxtTxt.append(sCade);
                edxtTxt.append("\n");
            }
            brReader.close();
        } catch (IOException e){e.printStackTrace();}
    }
}
