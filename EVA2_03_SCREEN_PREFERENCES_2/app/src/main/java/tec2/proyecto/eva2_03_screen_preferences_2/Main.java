package tec2.proyecto.eva2_03_screen_preferences_2;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

public class Main extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SharedPreferences spShare = PreferenceManager.getDefaultSharedPreferences(this);
        String sOpcion1 = spShare.getString("OPCION1","nada");
        Toast.makeText(this, sOpcion1, Toast.LENGTH_SHORT).show();
        boolean bOcpion2 = spShare.getBoolean("OPCION2",false);
        if (bOcpion2){
            Toast.makeText(this, "Verdadero", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Falso", Toast.LENGTH_SHORT).show();
        }
        boolean bOcpion3 = spShare.getBoolean("OPCION3",false);
        if (bOcpion3){
            Toast.makeText(this, "Verdadero", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Falso", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater miInflater = getMenuInflater();
        miInflater.inflate(R.menu.mis_menus, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.item1){
            Intent inLanzarConf = new Intent(this, Secondary.class);
            startActivity(inLanzarConf);
        }
        return super.onOptionsItemSelected(item);
    }
}
