package tec2.proyecto.eva2_07_objetos_archivos;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Main extends AppCompatActivity {

    EditText etNom, etApe, etEdad;
    RadioButton rbMale, rbFemale, rbRidiculo;
    CheckBox chbActive;
    Button btnSave, btnRead;
    TextView tvShow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        etNom = findViewById(R.id.edtxNom);
        etApe =findViewById(R.id.edtxApe);
        etEdad = findViewById(R.id.edtxEdad);
        rbMale = findViewById(R.id.rbMale);
        rbFemale = findViewById(R.id.rbFemale);
        rbRidiculo = findViewById(R.id.rbRidicule);
        chbActive = findViewById(R.id.chbxActivo);
        btnSave = findViewById(R.id.btnSave);
        btnRead = findViewById(R.id.btnRead);
        tvShow = findViewById(R.id.tvInfo);
    }

    public void onClickSave(View v){
        //LEER LOs WIDGETS
        String sNom = etNom.getText().toString();
        String sApe = etApe.getText().toString();
        int iEdad = Integer.parseInt(etEdad.getText().toString());
        boolean bStatus = chbActive.isChecked();
        int iSex;
        if (rbMale.isChecked()){
            iSex=0;
        } else if (rbFemale.isChecked()){
            iSex=1;
        } else {
            iSex = 2;
        }

        Datos dDatos = new Datos(sNom,sApe,iEdad,iSex,bStatus);

        try{
            FileOutputStream fosAbrir = openFileOutput("alola.nglou",0);
            ObjectOutputStream oosGuardar = new ObjectOutputStream(fosAbrir);
            oosGuardar.writeObject(dDatos);
            oosGuardar.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public void onClickRead(View v){
        FileInputStream fisAbrir = null;
        ObjectInputStream oisLeer = null;
        try {
            fisAbrir = openFileInput("alola.nglou");
            oisLeer = new ObjectInputStream(fisAbrir);
            Datos dDatoI = (Datos) oisLeer.readObject();
            while (true){
                tvShow.append("Nombre: "+ dDatoI.getsNombre() + "\n");
                tvShow.append("Apellido: "+ dDatoI.getsNombre() + "\n");
                tvShow.append("Edad: "+ dDatoI.getsNombre() + "\n");
                if (dDatoI.isbEstado()){
                    tvShow.append("Estudiante activo \n");
                } else {
                    tvShow.append("Estudiante inactivo \n");
                }
                switch (dDatoI.getiGenero()){
                    case 0:
                        tvShow.append("Genero masculino \n");
                        break;
                    case 1:
                        tvShow.append("Genero femenino \n");
                        break;
                    default:
                        tvShow.append("Genero no especificado...\n");
                        break;
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (fisAbrir != null){
                try {
                    fisAbrir.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}

class Datos implements Serializable{
    private String sNombre, sApellido;
    private int iEdad, iGenero;
    private boolean bEstado;

    public Datos(String sNombre, String sApellido, int iEdad, int iGenero, boolean bEstado) {
        this.sNombre = sNombre;
        this.sApellido = sApellido;
        this.iEdad = iEdad;
        this.iGenero = iGenero;
        this.bEstado = bEstado;
    }

    public Datos() {
        this.sNombre = "Angel Xavier";
        this.sApellido = "Cordova Urueta";
        this.iEdad = 21;
        this.iGenero = 0;
        this.bEstado = true;
    }

    public String getsNombre() {
        return sNombre;
    }

    public void setsNombre(String sNombre) {
        this.sNombre = sNombre;
    }

    public String getsApellido() {
        return sApellido;
    }

    public void setsApellido(String sApellido) {
        this.sApellido = sApellido;
    }

    public int getiEdad() {
        return iEdad;
    }

    public void setiEdad(int iEdad) {
        this.iEdad = iEdad;
    }

    public int getiGenero() {
        return iGenero;
    }

    public void setiGenero(int iGenero) {
        this.iGenero = iGenero;
    }

    public boolean isbEstado() {
        return bEstado;
    }

    public void setbEstado(boolean bEstado) {
        this.bEstado = bEstado;
    }
}
